"use strict";


// 1. Як можна створити рядок у JavaScript?
// Рядок у JavaScript можна створити двома способами, за допомогою лапок одинарних або подвійних: 
// const str = "string";
// const str2 = 'srting';
// А також викоритсовуючи конструктор String: 
// const str3 = new String("string")'

// 2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
// Між "" та '' не має різниці. Зі зворотніми лапками ми можемо використовувати шаблонний рядок.

// 3. Як перевірити, чи два рядки рівні між собою?
// За допомогою метода str.localeCompare(str2).

// 4. Що повертає Date.now()?
// Поточну дату.

// 5. Чим відрізняється Date.now() від new Date()?
//  new Date() поверне об'ект Date , а Date. now() - timestamp.

// 1. Перевірити, чи є рядок паліндромом.
// Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
// (читається однаково зліва направо і справа наліво), або false в іншому випадку.

const str1 = "радар";
const stringString = " моя мама";


const isPalindrom = (str) => {
    let str2 = "";

    for (let i = str.length - 1; i >= 0; i--) {
        str2 += str[i];
    }

    if (str.toLowerCase() === str2.toLowerCase()) {
        return console.log("Цей рядок - паліндром.");
    } else {
        return console.log("Цей рядок не паліндром.");
    }

}

console.log(isPalindrom(str1));
console.log(isPalindrom(stringString));


// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. 
// Ця функція стане в нагоді для валідації форми.

const string = "I love";

const checkStringLength = (str, maxLength) => str.length <= maxLength ? true : false
console.log(checkStringLength(string, 10));

// 3. Створіть функцію, яка визначає скільки повних років користувачу. 
// Отримайте дату народження користувача через prompt. 
// Функція повина повертати значення повних років на дату виклику функцію.

const userBirtday = prompt("Enter your birth date", "1988-8-20");


const getUserAge = (date => {
    let userAge = new Date(userBirtday).getFullYear();
    let now = new Date().getFullYear();
    return now - userAge;
});
console.log(getUserAge(userBirtday));
